#include <iostream>
#include <string>

#include "index.h"

namespace symtensor {

  Index::Index(char s, char l): _space{s}, _label{l} {}

  Index::Index(const std::string& s): _space{s[0]}, _label{s[1]} {}

  Index::Index(const Index &idx) {
    this->_space = idx.space();
    this->_label = idx.label();
  }

  char Index::space() const {
    return this->_space;
  }

  char Index::label() const {
    return this->_label;
  }

  unsigned Index::id() const {
    return (unsigned)this->_space*10 + (unsigned)this->_label;
  }

  // relational operators overload, defined as friend functions
  bool operator == (const Index &idx1, const Index &idx2) {
    return (idx1.id() == idx2.id());
  }

  bool operator != (const Index &idx1, const Index &idx2) {
    return !(idx1 == idx2);
  }

  bool operator >  (const Index &idx1, const Index &idx2) {
    if (idx1.space() < idx2.space()) {
      // a, b, c, ... : occupied space
      // i, j, k, ... : virtual space
      // occupied is considered smaller
      // that is: char a < char i means space(a) > space(i)
      return true;
    }
    else if (idx1.space() == idx2.space()) {
      return (idx1.label() > idx2.label());
    }
    return false;
  }

  bool operator < (const Index &idx1, const Index &idx2) {
    return !((idx1 == idx2) || (idx1 > idx2));
  }

  bool operator >= (const Index &idx1, const Index &idx2) {
    return ((idx1 > idx2) || (idx1 == idx2));
  }

  bool operator <= (const Index &idx1, const Index &idx2) {
    return ((idx1 < idx2) || (idx1 == idx2));
  }
  // end of relational operators

  // stream extraction operator overloading
  std::ostream &operator<<( std::ostream &out, const Index &indx) {
    out << indx.space() << indx.label();
    return out;
  }

} // namespace symtensor
