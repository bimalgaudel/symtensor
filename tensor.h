#ifndef SYMTENSOR_TENSOR_H
#define SYMTENSOR_TENSOR_H

#include <string>
#include <vector>
#include <iostream>

#include <btas/btas.h>

#include "index.h"

namespace symtensor {

  class Tensor {
    private:
      std::string _label; // 't', 'g', 'Gt', ...
      double _scale = 1.0;
      std::vector<Index> _occ_indx; // {Index('i', '1'), Index('i', '4'), ...}
      std::vector<Index> _virt_indx; // {Index('a', '2'), Index('a', '3'), ...}
      // a factor this tensor to be multiplied with while contracting
      // a btas::Tensor object associated to this object
      btas::Tensor<double> _btensor;

    public:
      Tensor() {}
      Tensor(std::string, const std::vector<Index> &,
                          const std::vector<Index> &);
      Tensor(std::string, const std::vector<Index> &, bool keep_order=false);
      // copy constructor
      Tensor(const Tensor &);
      //
      std::string label() const;
      //
      double scale() const;
      void set_scale(double s);
      //
      const std::vector<Index>& occ() const;
      const std::vector<Index>& virt() const;
      //
      std::vector<unsigned>  occ_id() const;
      std::vector<unsigned> virt_id() const;
      //
      const std::vector<Index> ov_space() const;
      const std::vector<unsigned> ov_id() const;
      //
      const btas::Tensor<double>& btensor() const;
      void add_btensor(const btas::Tensor<double> &);

      friend std::ostream &operator<<( std::ostream &, const Tensor &);
      friend Tensor contract(const Tensor &, const Tensor &);
  };

  std::vector<Index>    c_idx (const Tensor &, const Tensor &);
  std::vector<Index>    nc_idx(const Tensor &, const Tensor &);
  std::vector<unsigned> nc_id (const Tensor &, const Tensor &);
  Tensor contract(const Tensor &, const Tensor &);

} // namespace symtensor

#endif /* SYMTENSOR_TENSOR_H */
