#ifndef SYMTENSOR_INDEX_H
#define SYMTENSOR_INDEX_H

#include <iostream>
#include <string>

namespace symtensor {

  class Index {
    private:
      char _space; // 'i', 'a', ...
      char _label; // '1', '2', ...

    public:
      Index() {}
      Index(char, char);
      Index(const std::string &);
      // copy constructor
      Index(const Index &);
      char space() const;
      char label() const;
      unsigned id() const; // useful to call btas::contract

      // relational operators overloading
      friend bool operator == (const Index &, const Index &);
      friend bool operator != (const Index &, const Index &);
      friend bool operator >  (const Index &, const Index &);
      friend bool operator <  (const Index &, const Index &);
      friend bool operator >= (const Index &, const Index &);
      friend bool operator <= (const Index &, const Index &);

      // stream extraction operator overloading     
      friend std::ostream &operator<<( std::ostream &, const Index &);
  };

} // namespace symtensor

#endif /* SYMTENSOR_INDEX_H */
