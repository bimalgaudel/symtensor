#include <iostream>
#include <random>
#include <vector>
#include <btas/btas.h>

#include "tensor.h"
#include "index.h"

template <class T>
void print_vec(std::vector<T> v, bool new_line=false);

int main(int argc, char *argv[])
{
  using namespace std;
  using symtensor::Index;
  //
  typedef symtensor::Tensor CTensor;
  typedef btas::Tensor<double> DTensor;
  //
  //
  const auto n = 3;
  mt19937 rgen;
  auto dist = uniform_real_distribution<double>{-1.0, 1.0};
  //
  DTensor t1(n, n, n, n);
  DTensor t2(n, n, n, n);
  //
  t1.generate(bind(dist, rgen));
  t2.generate(bind(dist, mt19937(time(NULL))));
  //
  auto ct1 = CTensor("t1", vector<Index>{Index{'i', '1'}, Index{'j', '1'}},
                            vector<Index>{Index{'a', '1'}, Index{'b', '1'}});
  ct1.add_btensor(t1);
  auto ct2 = CTensor("t2", vector<Index>{Index{'i', '2'}, Index{'j', '1'}},
                            vector<Index>{Index{'a', '1'}, Index{'b', '2'}});
  ct2.add_btensor(t2);
  //
  cout << ct1 << endl;
  cout << ct2 << endl;
  //
  cout << endl;
  cout << "norm(t1) = ";
  cout << sqrt(btas::dot(t1, t1)) << endl;
  cout << "norm(t2) = ";
  cout << sqrt(btas::dot(t2, t2)) << endl;
  cout << endl;
  //
  cout << endl;
  cout << "Manually calling btas::contract.." << endl;
  DTensor result;
  btas::contract(1.0, t1, ct1.ov_id(),
                      t2, ct2.ov_id(), 
                 0.0, result, symtensor::nc_id(ct1, ct2));
  cout << "norm(t1*t2) = " << sqrt(btas::dot(result, result)) << endl;
  cout << endl;
  //
  auto ct12 = symtensor::contract(ct1, ct2);
  /* cout << ct12 << endl; */
  cout << "symtensor::contract figures out the tensor contraction on its own..." << endl;
  cout << "norm(ct1*ct2) = " << sqrt(btas::dot(ct12.btensor(),
                                      ct12.btensor())) << endl;
  cout << endl;
  //
  auto ct3 = CTensor("t3", vector<Index>{Index{"i2"}, Index{"j2"}},
                           vector<Index>{Index{"a2"}, Index{"b2"}});
  //
  auto ct4 = CTensor("t4", vector<Index>{Index{"i2"}, Index{"j2"},
                                         Index{"b2"}, Index{"a2"}});
  //
  auto ct5 = CTensor("t5", vector<Index>{Index{"i2"}, Index{"j2"},
                                         Index{"b2"}, Index{"a2"}}, true);
  /* cout << ct3 << endl; */
  /* cout << ct4 << endl; */
  /* cout << ct5 << endl; */
  //
  return 0;
}

template <class T>
void print_vec(std::vector<T> v, bool new_line) {
  using namespace std;
  for (auto i = v.begin(); i < v.end(); ++i) {
    cout << *i << " ";
    if (new_line) {
      cout << endl;
    }
  }
  if (!new_line) {
    cout << endl;
  }
}
