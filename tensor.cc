#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <btas/btas.h>

#include "index.h"
#include "tensor.h"

  using std::string;
  using std::vector;
  using std::ostream;
  using std::swap;
  using std::map;
  using std::pair;

namespace symtensor {

  Tensor::Tensor(string l, const vector<Index> &o,
                           const vector<Index> &v) {
    if (o.size() != v.size()) {
      throw "The dimensions of occupied and virtual space should match!";
    }
    // separate occupied and virtual spaces
    // and copy the passed indices to the class members
    const auto dim   = o.size();
    map<Index, Index> ov_map;
    for (auto i = 0; i < dim; ++i) {
      Index key   = o[i];
      Index value = v[i];
      if (key > value) {
        swap(key, value);
      }
      ov_map.insert(pair<Index, Index>(key, value));
    }
    // now we have indices in order
    // which should help us to map a given
    // tensor to the equivalent tensor that might be already
    // computed
    // copy the indices to the class members
    for (auto i = ov_map.begin(); i != ov_map.end(); ++i) {
      this->_occ_indx.push_back(i->first);
      this->_virt_indx.push_back(i->second);
    }
    // label of the tensor
    this->_label = l;
  }

  Tensor::Tensor(string l, const vector<Index> &all_space,
                           bool keep_order): Tensor() {
    // if a vector of indices is passed to the constructor
    // the vector should be divisible into occupied half
    // and the virtual half spaces
    auto size = all_space.size();
    if (size%2 != 0) {
      throw "The dimension of occupied and virtual space should match!";
    }
    vector<Index> ov;
    copy(all_space.begin(), all_space.end(), back_inserter(ov));
    if (!keep_order) {
      sort(ov.begin(), ov.end());
    }
    //
    vector<Index> occs;
    vector<Index> virts;
    size /= 2;
    copy(ov.begin(), ov.begin()+size, back_inserter(occs));
    copy(ov.begin()+size, ov.end(), back_inserter(virts));
    //
    if (!keep_order) {
      this->_label     = l;
      this->_occ_indx  = occs;
      this->_virt_indx = virts;
    }
    else {
      auto temp        = Tensor(l, occs, virts);
      this->_label     = temp._label;
      this->_occ_indx  = temp._occ_indx;
      this->_virt_indx = temp._virt_indx;
    }
  }

  Tensor::Tensor(const Tensor &t) {
    // copy constructor
    this->_label     = t._label;
    this->_occ_indx  = t._occ_indx;
    this->_virt_indx = t._virt_indx;
    this->_scale     = t._scale;
    this->_btensor   = t._btensor;
  }

  string Tensor::label() const {
    return this->_label;
  }

  double Tensor::scale() const {
    return this->_scale;
  }

  void Tensor::set_scale(double d) {
    this->_scale = d;
  }

  const vector<Index>& Tensor::occ() const {
    return this->_occ_indx;
  }

  const vector<Index>& Tensor::virt() const {
    return this->_virt_indx;
  }

  vector<unsigned>  Tensor::occ_id() const {
  // get a vector of ids of occupied indices
    vector<unsigned> ids;
    for (auto e: this->occ()) {
      ids.push_back(e.id());
    }
    return ids;
  }

  vector<unsigned> Tensor::virt_id() const {
    // get a vector of ids of virtual indices
    vector<unsigned> ids;
    for (auto e: this->virt()) {
      ids.push_back(e.id());
    }
    return ids;
  }

  const vector<Index> Tensor::ov_space() const {
    // get a combined vector of occupied and virtual Index objects
    vector<Index> space;
    copy(this->_occ_indx.begin(), this->_occ_indx.end(),
                                  back_inserter(space));
    copy(this->_virt_indx.begin(), this->_virt_indx.end(),
                                    back_inserter(space));
    return space;
  }

  const vector<unsigned> Tensor::ov_id() const {
    // combined vector of occupied and virtual Index object's id()'s
    // useful to call btas::contract
    vector<unsigned> ids;
    for (auto i: this->ov_space()) {
      ids.push_back(i.id());
    }
    return ids;
  }

  const btas::Tensor<double>& Tensor::btensor() const {
    return this->_btensor;
  }

  void Tensor::add_btensor(const btas::Tensor<double> &bt) {
    this->_btensor = bt;
  }

  ostream &operator<<( ostream &out, const Tensor &tnsr) {
    out << tnsr.label();
    out << "{";
    for (auto i: tnsr.occ()) {
      out << i;
      out << " ";
    }
    for (auto i: tnsr.virt()) {
      out << " ";
      out << i;
    }
    out << "}";
    return out;
  }

  vector<Index> c_idx(const Tensor &t1, const Tensor &t2) {
    // get a vector of Index from two Tensors which are common
    // to both and thus will be contracted during btas::contract
    vector<Index> idx;
    auto t1_ov = t1.ov_space();
    auto t2_ov = t2.ov_space();
    set_intersection(t1_ov.begin(), t1_ov.end(),
                     t2_ov.begin(), t2_ov.end(),
                     back_inserter(idx));
    return idx;
  }

  vector<Index> nc_idx(const Tensor &t1, const Tensor &t2) {
    // get a vector of those Index objects from two Tensors
    // which won't get contracted: they have to come in the
    // same order in which we should store the btas:Tensor
    // that results from the contraction of such Tensors
    vector<Index> idx;
    auto t1_ov = t1.ov_space();
    auto t2_ov = t2.ov_space();
    set_symmetric_difference(t1_ov.begin(), t1_ov.end(),
                              t2_ov.begin(), t2_ov.end(),
                              back_inserter(idx));
    sort(idx.begin(), idx.end());
    return idx;
  }

  vector<unsigned> nc_id(const Tensor &t1, const Tensor &t2) {
    auto idx = nc_idx(t1, t2);
    vector<unsigned> ids;
    for (auto i: idx) {
      ids.push_back(i.id());
    }
    return ids;
  }

  Tensor contract(const Tensor &t1, const Tensor &t2) {
    // contract two tensors
    // TODO
    // if (t1 or t2 is empty)
    //  throw "Can't contract Tensors with empty btas::Tensor!";
    // t1, t2 and non-contracting indices
    auto t1_ov = t1.ov_id();
    auto t2_ov = t2.ov_id();
    auto uc_id = nc_id(t1, t2);
    auto factor = t1.scale()*t2.scale();
    auto l = t1.label() + t2.label();
    auto result = Tensor(l, nc_idx(t1, t2));
    // call the btas::contract
    btas::contract(factor, t1._btensor, t1_ov,
                           t2._btensor, t2_ov,
                           0.0, result._btensor, uc_id);
    return result;
  }
   
} // namespace symtensor
