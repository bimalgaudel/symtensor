#include <iostream>
#include <string>
#include <vector>
#include <map>

#include "index.h"
#include "tensor.h"

using namespace std;
using namespace symtensor;

template <class T>
void print_vec(vector<T> v, bool new_line=false);

void t_translate(const Tensor &);

int main(int argc, char *argv[])
{
    // creating some Index objects
    vector<Index> id1;
    vector<char> spaces{'a', 'i', 'b', 'j'};
    vector<char> labels{'4', '2', '3', '1'};
    for (auto i: spaces) {
        for (auto j: labels) {
            id1.push_back(Index(i, j));
        }
    }
     
    // create 5 dimensional occupied and virtual space
    const size_t dim = 5;
    vector<Index>  occs(dim);
    vector<Index> virts(dim);
    for (auto i = 0; i < dim; ++i) {
        occs[i] = id1[i];
    }
    for (auto i = 0; i < dim; ++i) {
       virts[i] = id1[id1.size()-(1+i)];
    }
    // mangling a bit so occs and virts end up having mixed spaces
    // useful for testing purpose
    swap(occs[0], virts[0]);
    swap(occs[2], virts[2]);
    swap(occs[4], virts[4]);
    //
    cout << "\nSuppose we get a tensor T1 of the form..\n" << endl;
    cout << "  "; print_vec(virts);
    cout << "T1" << endl;
    cout << "  "; print_vec(occs);
    cout << "\nwhich is equivalently translated into a\nsymtensor::Tensor object as:\n" << endl;
    Tensor T1{"T1", occs, virts};
    cout << "  "; print_vec(T1.virt());
    cout << "T1" << endl;
    cout << "  "; print_vec(T1.occ()); cout << endl;
    cout << "T1 also translates into: ";
    t_translate(T1);
    cout << "  which is useful if we want to map it to an existing Tensor\n" << endl;
    //
    cout << "Creating T2 tensor passing two vectors of Index objects\n" << endl;
    auto T2 = Tensor{"T2", vector<Index>{Index{'b', '2'}, Index{'j', '3'}},
                           vector<Index>{Index{'i', '1'}, Index{'b', '4'}}};
    cout << T2 << ": ";
    t_translate(T2);
    //
    cout << "\nCreating T3 tensor passing single vector of Index objects\n";
    auto T3 = Tensor{"T3", vector<Index>{Index{'b', '3'},
                                        Index{'a', '1'},
                                        Index{'i', '2'},
                                        Index{'b', '2'}}};
    cout << T3 << ": ";
    t_translate(T3);
    return 0;
}

void t_translate(const Tensor &tnsr) {
    string result = tnsr.label() + "_";
    for (auto o: tnsr.occ()) {
        result += (o.space() >= 'i')? "o": "v";
    }
    for (auto v: tnsr.virt()) {
        result += (v.space() <  'i')? "v": "o";
    }
    cout << result << endl;
}

template <class T>
void print_vec(vector<T> v, bool new_line) {
    for (auto i = v.begin(); i < v.end(); ++i) {
        cout << *i << " ";
        if (new_line) {
            cout << endl;
        }
    }
    if (!new_line) {
        cout << endl;
    }
}
